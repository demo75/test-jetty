class CreateTripStops < ActiveRecord::Migration[6.0]
  def change
    create_table :trip_stops do |t|
      t.string :name
      t.decimal :latitude
      t.decimal :longitude
      t.string :address
      t.datetime :departure_time
      t.boolean :boarding
      t.references :trip, foreign_key: true
      t.timestamps
    end
  end
end
