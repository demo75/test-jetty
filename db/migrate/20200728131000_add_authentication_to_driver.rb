class AddAuthenticationToDriver < ActiveRecord::Migration[6.0]
  def change
    add_column :drivers, :authentication_email, :string
    add_column :drivers, :authentication_token, :string
  end
end
