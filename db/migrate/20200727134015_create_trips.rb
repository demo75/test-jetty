class CreateTrips < ActiveRecord::Migration[6.0]
  def change
    create_table :trips do |t|
      t.datetime :date
      t.decimal :price
      t.string :name
      t.string :status
      t.references :driver, foreign_key: true
      t.timestamps
    end
  end
end
