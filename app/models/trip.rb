class Trip < ApplicationRecord
  belongs_to :driver
  has_many :trip_stops
  #group by day
  #scope :ordered, ->{order(created_at: :asc).group_by{ |t| t.created_at.day}}
  #only order
  scope :ordered, ->{order(created_at: :asc)}

end
