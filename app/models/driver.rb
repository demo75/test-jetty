class Driver < ApplicationRecord
  require 'rest-client'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :trips
  
  def authenticate_api(pass, email)
    url= "https://jettymx-st.herokuapp.com/api/drivers/session"
    response = RestClient.post url, {driver: {email: email, password: pass}}.to_json, {content_type: :json, accept: :json}
    response = JSON.parse(response.to_str)
    if authentication_email.nil?
      update(authentication_email: response["email"], authentication_token: response["auth_token"])
    else
      update(authentication_token: response["auth_token"])
    end
  rescue RestClient::Exception
    "No autorizado"
  end

  def get_data_from_api
    url= "https://jettymx-st.herokuapp.com/api/drivers/trips"
    response = RestClient.get url, {authorization: "Token #{authentication_token}, email=#{authentication_email}"}
  rescue RestClient::Exception
    false
  end
end
