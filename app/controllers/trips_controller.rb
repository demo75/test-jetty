class TripsController < ApplicationController
  def index
    @trips =[]
    Trip.ordered.each do |trip|
      @trips << {id: trip.id, name:trip.name, price: trip.price, date: trip.date.strftime("%Y-%m-%d"), time: trip.date.strftime("%I:%M %p"),
                driver: trip.driver,
                trip_stops: trip.trip_stops}
    end
    
    @trips = @trips.to_json
  end

  def api_data
    if current_driver.get_data_from_api
      @trips = current_driver.get_data_from_api
    else
      sign_out
      redirect_to root_path 
    end
  end
end