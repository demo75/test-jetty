class ApplicationController < ActionController::Base
  before_action :authenticate_driver!

  protected
    def after_sign_in_path_for(resource)
      resource.authenticate_api(params[:driver][:password], params[:driver][:email])
      root_path
    end
end
