require 'test_helper'

class DriverTest < ActiveSupport::TestCase

  test "check api connection_not_authorized" do
    driver = Driver.first
    assert_equal 'No autorizado', driver.authenticate_api("zxcvbasdf", "driver+test500@example.coms")
  end

  test "check api connection_authorized" do
    driver = Driver.first
    assert_equal true, driver.authenticate_api("zxcvbasdf", "driver+test500@example.com")
  end

  test "check api get_data_true" do
    #cambiar el token en los fixtures para obtener true
    driver = Driver.first
    assert_equal true, driver.get_data_from_api
  end

  test "check api get_data_error" do
    driver = Driver.first
    assert_equal false, driver.get_data_from_api
  end

end
