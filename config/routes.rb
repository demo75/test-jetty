Rails.application.routes.draw do
  devise_for :drivers
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :trips, only: [:index] do
    collection do
      get :api_data
    end
  end
  root 'trips#api_data'
end
